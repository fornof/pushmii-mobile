/**
 * @format
 */
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { PaperProvider,MD3LightTheme as DefaultTheme, } from 'react-native-paper';
import {ProfileScreen} from './src/components/Home'
const Stack = createNativeStackNavigator();
const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'tomato',
      secondary: 'yellow',
    },
  };



export default function Main() {
  return (
//      <NavigationContainer>
 
//    <Stack.Navigator>
//         <Stack.Screen
//           name="Home"
//            component={App}
//            options={{title: 'Welcome'}}
//          />
//          <Stack.Screen name="Profile" component={ProfileScreen} />
//        </Stack.Navigator>
//      </NavigationContainer>
//<SafeAreaProvider><App/></SafeAreaProvider> // elements
<PaperProvider theme={{ version: 2 }}>
    <NavigationContainer>
        <App />
    </NavigationContainer>
</PaperProvider>
    
  );
}

//AppRegistry.registerComponent(appName, () => Main);



AppRegistry.registerComponent(appName, () => Main);
