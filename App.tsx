/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
//import { useNavigation } from '@react-navigation/native';
import * as permissions from 'react-native-permissions';
import React, { useState } from 'react';
import axios from 'axios';
import type {LegacyRef, PropsWithChildren} from 'react';
import {check, PERMISSIONS, request, RESULTS} from 'react-native-permissions';
//import { IconButton, MD3Colors } from 'react-native-paper';
const fire = require('./src/img/fire.jpg')
//todo: delete from package.json import { Text , Icon } from 'galio-framework'
import { useTheme, Button } from 'react-native-paper';
import {
  
  
  Image,
  Text,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,

  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Album from './src/components/Album';
import { RootNavigator } from './src/components/Drawer';
//import { ButtonGroup, Icon, Text, Image } from '@rneui/base';

type SectionProps = PropsWithChildren<{
  title: string;
}>;


async function checkPhotoPermission() {
  return check(PERMISSIONS.IOS.PHOTO_LIBRARY)
  
  
}



function Section({children, title}: SectionProps): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';
  return (

    <View style={styles.sectionContainer}>
   <Image source={fire}  style={{width: 50, height: 50}} />
<Text>Copy to clipboard</Text>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
}
const btnGroup = function () {
  // const [selectedIndex, setSelectedIndex] = React.useState(0);
  return (
    <View>
      {/* {RootNavigator()} */}
    {/* <ButtonGroup
      buttonStyle={{ padding: 10 }}
      selectedButtonStyle={{ backgroundColor: '#e2e2e2' }}
      buttons={[
        <Icon name="format-align-left" />,
        <Icon name="format-align-center" />,
        <Icon name="format-align-right" />,
      ]}
      selectedIndex={selectedIndex}
      onPress={setSelectedIndex}
    /> */}
    <Button icon="camera" mode="contained" onPress={() => console.log('Pressed')}>
  <Text>Press me</Text>
  </Button>
  
    </View>
  );
}

function App(): JSX.Element {
  const theme = useTheme();

 
  const [dataFromResponse,setDataFromResponse] = useState('')
  const isDarkMode = useColorScheme() === 'dark';
  const clicker = async () => {
    console.log('clicked');
    let response = await request(PERMISSIONS.IOS.PHOTO_LIBRARY)
    let permission = await checkPhotoPermission()
    if(permission === RESULTS.GRANTED) {
      //UseCameraRoll()
    }
    console.log(response, 'response')
    //let response = (await axios.get('https://jsonplaceholder.typicode.com/todos/1')).data
    setDataFromResponse(response as string)
  }
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };


  
 //const navigator:any = useNavigation();
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <Section title="Step One">
            Edit <Text style={styles.highlight}>App.tsx</Text> to change this
            screen and then come back to see your edits. Robert Rocks
            Hi
            <Text>{JSON.stringify(dataFromResponse)}</Text>
          </Section>
            <Button onPress={clicker} > Click Meee</Button>
           
          <Section title="See Your Changes">
            <ReloadInstructions />
          </Section>
          <Section title="Debug">
            <DebugInstructions />
          </Section>
          <Section title="Learn More">
            Read the docs to discover what to do next:
            {/* <Button icon={{ uri: 'https://avatars0.githubusercontent.com/u/17571969?v=3&s=400' }}>
  Press me
</Button> */}
<TouchableOpacity activeOpacity={0.5}>
< Text p muted >
         Hi, I'm a Galio component
</ Text>
</TouchableOpacity>
<View style={{ backgroundColor: theme.colors.primary }} />; 
            {/* <Album navigator={navigator}/> */}
            <Album/>
            {btnGroup()}
          </Section>
          <LearnMoreLinks />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
