module.exports = {
  presets: ['module:metro-react-native-babel-preset',"@babel/preset-flow"],
  env: {
    production: {
      plugins: ['react-native-paper/babel', 'react-native-reanimated/plugin'],
    },
  },
};
// react-native-reanimated/plugin has to be listed last